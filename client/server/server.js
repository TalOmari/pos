var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var db=require('./db')
var CashiersRouter=require('./resources/Cashier/CashierRouter');
var LogsRouter=require('./resources/Logs/LogsRouter')
var TransactionRouter=require('./resources/Transaction/TransactionRouter')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(__dirname+'/../dist/client'));

app.get('/',function(req,res){
	res.sendFile('index.html')
})


app.use('/cashier',CashiersRouter)
app.use('/Logs',LogsRouter)
app.use('/Transaction',TransactionRouter)
app.get('*', (req, res)=> {
	res.sendFile(path.join(__dirname,'../dist/client/index.html'))
})

module.exports=app
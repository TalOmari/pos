import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import axios from 'axios'
@Component({
  selector: 'app-transaction-by-cashier',
  templateUrl: './transaction-by-cashier.component.html',
  styleUrls: ['./transaction-by-cashier.component.css']
})
export class TransactionByCashierComponent implements OnInit {
  private cost: number;
  private paid: number;
  private remain: number;
  moneyType=[1,5,10,20,50]
  constructor(private router: Router) { }

  ngOnInit() {
  }

  	cal(){
	this.remain=this.paid-this.cost
  	}
  
  save(){
  	var c=Number(this.cost);
  	var p=Number(this.paid);
  	var r=p-c;
  	this.remain=this.paid-this.cost
  	console.log(this.remain)
  	var data=[[c,p,r,1]]
  	axios({
  		method:'post',
  		url:'Logs/CreateLogs',
  		data:data
  	})
  	.then(function(response){
  		console.log(response)
  	})

  	var d=0;
  	var obj={};
  	while(r>0){
  		for(var i=4;i>=0;i--){
  			if(r>=this.moneyType[i]){
  				d=this.moneyType[i];
  				break;
  			}
  		}
  		r=r-d;
  		if(obj[d]){
  			obj[d]+=1;
  		}else{
  			obj[d]=1;
  		}	
  	}
  	console.log('ssssss ',obj)
  	var array=[];
  	var array2=[];
  	
  	array.push(['W',1,p,1])
  	for(var k in obj){
  		array2=['D',obj[k],k,1]
  		array.push(array2)
  	}
  	axios({
  		method:'post',
  		url:'Transaction/Create',
  		data:array
  	})
  	.then(function(response){
  		console.log(response)
  	})

  	}
  }
}

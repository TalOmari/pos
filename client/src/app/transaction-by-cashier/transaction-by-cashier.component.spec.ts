import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionByCashierComponent } from './transaction-by-cashier.component';

describe('TransactionByCashierComponent', () => {
  let component: TransactionByCashierComponent;
  let fixture: ComponentFixture<TransactionByCashierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionByCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionByCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

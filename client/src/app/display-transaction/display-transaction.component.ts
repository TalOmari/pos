import { Component, OnInit } from '@angular/core';
import axios from 'axios' 
@Component({
  selector: 'app-display-transaction',
  templateUrl: './display-transaction.component.html',
  styleUrls: ['./display-transaction.component.css']
})
export class DisplayTransactionComponent implements OnInit {
  public cashiers: cashiersInterface[];
  public logs: logsInterface[];
  constructor() { }

  ngOnInit() {
  	var that=this;
  	axios({
  		method:'get',
  		url:'cashier/Retriveall'
  	})
  	.then(function(response){
  		console.log(response.data)
  		that.cashiers=response.data
  	})

  }

  getDataofTransaction(id): void{
  	var that =this
  	console.log(id)
  	axios({
  		method:'get',
  		url:`Logs/RetriveLogs/${id}`
  	})
  	.then(function(response){
  		console.log(response.data)
  		that.logs=response.data
  	})
  }

}

interface cashiersInterface {
	firstName: string;
	lastName: string;
	casheirId: number;
	email: string;
	userName: string;
}

interface logsInterface {
	cashierId:number;
	dates:string;
	cost:number;
	Paied:number;
	changes:number;
}
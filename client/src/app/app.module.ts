import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TransactionByCashierComponent } from './transaction-by-cashier/transaction-by-cashier.component';
import { HomeComponent } from './home/home.component';
import { DisplayTransactionComponent } from './display-transaction/display-transaction.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TransactionByCashierComponent,
    HomeComponent,
    DisplayTransactionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
    	{path: '', component: HomeComponent, pathMatch:'full'},
    	{path: 'login', component: LoginComponent},
    	{path: 'transByCash', component: TransactionByCashierComponent},
    	{path: 'display', component: DisplayTransactionComponent},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
